# Descript Machine Specification

## Assignment

☐ `num: 1, num @lineOut`
☐ `:num + 1, num @lineOut`
☐ `inc: {@it + 1}, :num inc, num @lineOut`
☐ `{..num: @it + 1} 5, num @lineOut`
☐ `{..num: 3.35}!, num @lineOut`

## System

☐ `num @toInteger @lineOut`

## Arrays

☐ `[1, 2, 3] @lineOut`
☐ `4.5 [@toInteger, @toFloat] @lineOut`
☐ `[1, 2, 3].map inc @lineOut`
☐ `max: {@it.map {@it > ..x ? ..x: @it}, x}, [1, 2, 3] max @lineOut`
