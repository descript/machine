var tType = require('../types/type');
var tExpression = require('../types/expression');
var tExecution = require('../types/execution');
var tName = require('../types/name');
var tString = require('../types/string');
var tInteger = require('../types/integer');
var tResolve = require('../types/resolve');

var oCombine = module.exports = {
  combine: function (execution, left, right) {
    if (tName.is(left)) {
      left = tResolve.resolve(execution.context, left);
    }

    if (tName.is(right)) {
      right = tResolve.resolve(execution.context, right);
    }

    var leftExecution, rightExecution;

    if (tExpression.is(left)) {
      leftExecution = left.execute(execution.context);
    }

    if (tExpression.is(right)) {
      rightExecution = right.execute(execution.context);
    }

    if (leftExecution || rightExecution) {
      return tExecution.all(leftExecution, rightExecution).then(function (leftResult, rightResult) {
        return oCombine.combine(execution, leftResult, rightResult);
      });
    }

    if (tString.is(left)) {
      if (tString.is(right)) {
        execution.value = left + right;
        return;
      }

      if (tInteger.is(right)) {
        execution.value = left + right.toString();
        return;
      }

      throw new TypeError('Cannot combine string and ' + tType.of(right));
    }
  }
};
