module.exports = {
  add: function (execution, left, right) {
    if (typeof left !== 'number' || typeof right !== 'number') {
      throw new TypeError('value must be a number');
    }
    execution.value = left + right;
  },

  subtract: function (execution, left, right) {
    if (typeof left !== 'number' || typeof right !== 'number') {
      throw new TypeError('value must be a number');
    }
    execution.value = left - right;
  },

  multiply: function (execution, left, right) {
    if (typeof left !== 'number' || typeof right !== 'number') {
      throw new TypeError('value must be a number');
    }
    execution.value = left * right;
  },

  divide: function (execution, left, right) {
    if (typeof left !== 'number' || typeof right !== 'number') {
      throw new TypeError('value must be a number');
    }
    execution.value = left / right;
  },

  modulus: function (execution, left, right) {
    if (typeof left !== 'number' || typeof right !== 'number') {
      throw new TypeError('value must be a number');
    }
    execution.value = left % right;
  },

  power: function (execution, left, right) {
    if (typeof left !== 'number' || typeof right !== 'number') {
      throw new TypeError('value must be a number');
    }
    execution.value = Math.pow(left, right);
  },

  root: function (execution, left, right) {
    if (typeof left !== 'number' || typeof right !== 'number') {
      throw new TypeError('value must be a number');
    }
    execution.value = Math.pow(left, 1/right);
  }
};
