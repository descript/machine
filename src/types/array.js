var tAny = require('./any');
var tType = require('./type');

module.exports = tType.new('array', {
  new: [[], function () {
    return [];
  }],
  is: [[tAny], function (prototype, candidate) {
    return Array.isArray(candidate);
  }]
});
