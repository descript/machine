var tAny = require('./any');
var tType = require('./type');

module.exports = tType.new('integer', {
  new: [[], function () {
    return 0;
  }],
  is: [[tAny], function (prototype, candidate) {
    return typeof candidate === 'number' && parseInt(candidate) === candidate;
  }]
});
