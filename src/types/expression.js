var tAny = require('./any');
var tType = require('./type');
var tRoot = require('./root');
var tBreak = require('./break');

var _break;

module.exports = tType.new('expression', {
  new: [[tAny.multiple], function (prototype, value) {
    var expression = Object.create(prototype);
    expression.value = value;
    return expression;
  }],
  is: [[tAny], function (prototype, candidate) {
    return typeof candidate === 'object' &&
      Object.getPrototypeOf(candidate).type.id === prototype.type.id;
  }]
}, {
  execute: [[tRoot], function (self, context) {
    var execution = tType.registry.execution.new(self, context);
    setTimeout(function () {
      execution.start();
    });
    return execution;
  }]
});
