var tAny = require('./any');
var tType = require('./type');

module.exports = tType.new('string', {
  new: [[], function () {
    return '';
  }],
  is: [[tAny], function (prototype, candidate) {
    return typeof candidate === 'string';
  }]
});
