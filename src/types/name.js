var tAny = require('./any');
var tString = require('./string');
var tType = require('./type');

module.exports = tType.new('name', {
  new: [[tString.multiple], function (prototype, value) {
    var name = Object.create(prototype);
    name.name = value;
    return name;
  }],
  is: [[tAny], function (prototype, candidate) {
    return typeof candidate === 'object' &&
      Object.getPrototypeOf(candidate).type.id === prototype.type.id;
  }]
});
