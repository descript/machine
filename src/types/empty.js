module.exports = Object.create({
  type: -1,
  is: function (candidate) {
    return candidate === module.exports;
  }
});

module.exports.empty = null;
