var tAny = require('./any');
var tName = require('./name');
var tType = require('./type');
var tEmpty = require('./empty');

module.exports = tType.new('resolve', {
  resolve: [[tAny, tName], function (prototype, context, name) {
    var value = tEmpty;
    name.name.forEach(function (segment, i) {
      if (context === tEmpty) {
        return;
      }
      if (i === name.name.length - 1) {
        if (segment in context) {
          value = context[segment];
        }
      }
      else {
        if (segment in context) {
          context = context[segment];
        }
        else {
          context = tEmpty;
        }
      }
    });
    return value;
  }],
  is: [[tAny], function (prototype, candidate) {
    return false;
  }]
});
