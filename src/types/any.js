var tType = require('./type');

var tAny = module.exports = tType.new('any', {
  new: [[], function () {
    return;
  }]
});

tAny.is = function () {
  return true;
};
