var tAny = require('./any');
var tType = require('./type');
var tName = require('./name');
var tEmpty = require('./empty');

var set = function (execution) {
  var context = execution.context;
  if (tName.is(execution.key)) {
    execution.key.name.forEach(function (segment, i) {
      if (i === execution.key.name.length - 1) {
        context[segment] = execution.value;
      }
      else {
        if (segment in context) {
          context = context[segment];
        }
        else {
          context = context[segment] = {};
        }
      }
    });
  }

  else {
    context[execution.key] = execution.value;
  }

  execution.value = tEmpty;
}

module.exports = tType.new('root', {
  new: [[], function (prototype) {
    return Object.create(prototype);
  }],
  is: [[tAny], function (prototype, candidate) {
    return typeof candidate === 'object' &&
      Object.getPrototypeOf(candidate).type.id === prototype.type.id;
  }]
}, {
  instructionHandlers: [[], function (self) {
    return {
      ':': function (execution, instruction) {
        execution.key = execution.value;
        execution.value = tEmpty;
        execution.onBreak = set;
      }
    };
  }]
});
