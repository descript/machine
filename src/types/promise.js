var tAny = require('./any');
var tType = require('./type');
var tFunction = require('./function');

var tPromise = module.exports = tType.new('promise', function () {
  return {
    new: [[], function (prototype) {
      var _promise = Object.create(prototype);
      _promise.actionHandlers = [];
      return _promise;
    }],
    is: [[tAny], function (prototype, candidate) {
      return typeof candidate === 'object' &&
        Object.getPrototypeOf(candidate).type.id === prototype.type.id;
    }],
    all: [[tPromise.multiple, tFunction], function (prototype, promises, fn) {
      console.log(promises, fn);
    }]
  };
}, {
  then: [[tFunction], function (self, fn) {
    self.actionHandlers.push(fn);
    return self;
  }]
});
