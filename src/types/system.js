var tAny = require('./any');
var tType = require('./type');
var tString = require('./string');
var tInteger = require('./integer');

module.exports = tType.new('system', {
  new: [[], function (prototype) {
    return Object.create(prototype);
  }],
  is: [[tAny], function (prototype, candidate) {
    return typeof candidate === 'object' &&
      Object.getPrototypeOf(candidate).type.id === prototype.type.id;
  }]
}, {
  lineOut: [[tString], function (self, it) {
    console.log(it);
  }],

  errorLineOut: [[tString], function (self, it) {
    console.error(it);
  }],

  toString: [[tAny], function (self, it) {
    return String(it);
  }],

  toInteger: [[tString, tInteger.optional], function (self, it, base) {
    return parseInt(it, base);
  }],

  toFloat: [[tString, tInteger.optional], function (self, it, base) {
    return parseFloat(it, base);
  }]
});
