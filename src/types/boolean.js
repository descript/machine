var tAny = require('./any');
var tType = require('./type');

module.exports = tType.new('boolean', {
  new: [[], function () {
    return false;
  }],
  is: [[tAny], function (prototype, candidate) {
    return typeof candidate === 'boolean';
  }]
});
