var tAny = require('./any');
var tString = require('./string');
var tType = require('./type');

module.exports = tType.new('break', {
  new: [[], function (prototype) {
    var _break = Object.create(prototype);
    _break.break = null;
    return _break;
  }],
  is: [[tAny], function (prototype, candidate) {
    return typeof candidate === 'object' &&
      Object.getPrototypeOf(candidate).type.id === prototype.type.id;
  }]
});
