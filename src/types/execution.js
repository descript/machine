var tAny = require('./any');
var tRoot = require('./root');
var tType = require('./type');
var tName = require('./name');
var tBreak = require('./break');
var tExpression = require('./expression');
var tEmpty = require('./empty');
var tFunction = require('./function');
var tPromise = require('./promise');

var schedule = [];
var scheduleTimer = null;
var runSchedule = function () {
  if (scheduleTimer === null) {
    scheduleTimer = setTimeout(function () {
      scheduleTimer = null;
      var next = schedule.shift();
      next.seek();
      if (schedule.length > 0) {
        runSchedule();
      }
    });
  }
};

var tExecution = module.exports = tType.new('execution', {
  new: [[tExpression, tRoot], function (prototype, expression, context) {
    var execution = Object.create(prototype);
    execution.context = context;
    execution.expression = expression;
    execution.pointer = 0;
    execution.length = expression.value.length;
    execution.complete = false;
    execution.instructionHandlers = context.instructionHandlers();
    execution.value = tEmpty;
    execution.lastValue = tEmpty;
    execution.actionHandlers = [];
    return execution;
  }],
  is: [[tAny], function (prototype, candidate) {
    return typeof candidate === 'object' &&
      Object.getPrototypeOf(candidate).type.id === prototype.type.id;
  }],
  all: [[tAny.multiple], function (prototype, args) {
    var actionHandlers = [];
    var counter = 0;
    var currentArgs = args;
    var check = function (value) {
      if (--counter === 0) {
        actionHandlers.forEach(function (fn) {
          currentArgs = [fn.apply(null, currentArgs.map(function (arg) {
            if (tExecution.is(arg)) {
              return arg.lastValue;
            }
            return arg;
          }))];
        })
      }
    };
    args.forEach(function (arg) {
      if (tExecution.is(arg)) {
        arg.then(check);
        counter++;
      }
    });
    var promise = tPromise.new();
    return promise;
  }]
}, {
  then: [[tFunction], function (self, fn) {
    self.actionHandlers.push(fn);
    return self;
  }],
  start: [[], function (self) {
    self.seek();
  }],
  seek: [[], function (self) {
    var tOperation = tType.registry.operation;
    var left = tEmpty;
    var operation = tOperation.default;

    var reset = function () {
      var promise;
      if (left !== tEmpty && self.value !== tEmpty) {
        promise = operation.apply(self, self.value, left);
      }

      else {
        self.value = left;
      }

      left = tEmpty;
      operation = tOperation.default;

      if (promise) {
        promise.then(function () {
          schedule.push(self);
          runSchedule();
        });
        return;
      }

      return promise;
    };

    while (self.pointer <= self.length) {
      var instruction = self.expression.value[self.pointer];
      self.pointer++;

      if (tBreak.is(instruction) || typeof instruction === 'undefined') {
        var promise = reset();
        if (promise) {
          self.pointer--;
          return;
        }

        if (typeof self.onBreak === 'function') {
          self.onBreak(self);
        }
        self.lastValue = self.value;
        self.value = tEmpty;
        continue;
      }

      if (tOperation.is(instruction) && instruction.operation in self.instructionHandlers) {
        var promise = reset();
        if (promise) {
          self.pointer--;
          return;
        }

        self.instructionHandlers[instruction.operation](self, instruction);
        schedule.push(self);
        runSchedule();
        return;
      }

      if (tFunction.is(instruction)) {
        self.value = instruction(self.value);
        continue;
      }

      if (left !== tEmpty &&
        operation === tOperation.default &&
        tOperation.is(instruction)) {
        operation = instruction;
        continue;
      }

      if (left === tEmpty &&
        operation === tOperation.default) {
        left = instruction;
        continue;
      }

      var promise = operation.apply(self, left, instruction);
      if (promise) {
        promise.then(function () {
          schedule.push(self);
          runSchedule();
        });
        return;
      }
    }

    self.complete = true;
    self.actionHandlers.forEach(function (fn) {
      fn(self);
    });
  }]
});
