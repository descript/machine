var tAny = require('./any');
var tType = require('./type');

module.exports = tType.new('function', {
  is: [[tAny], function (prototype, candidate) {
    return typeof candidate === 'function';
  }]
});
