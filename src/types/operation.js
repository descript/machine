var tAny = require('./any');
var tType = require('./type');
var tString = require('./string');
var tFunction = require('./function');
var tExecution = require('./execution');

var operationRegistry = {};

var tOperation = module.exports = tType.new('operation', {
  new: [[tString], function (prototype, value) {
    if (value in operationRegistry) {
      return operationRegistry[value];
    }
    var operation = operationRegistry[value] = Object.create(prototype);
    operation.operation = value;
    return operation;
  }],
  is: [[tAny], function (prototype, candidate) {
    return typeof candidate === 'object' &&
      Object.getPrototypeOf(candidate).type.id === prototype.type.id;
  }],
  register: [[tString, tFunction], function (prototype, symbol, fn) {
    var operation = tOperation.new(symbol);
    operation.action = fn;
  }]
}, {
  apply: [[tExecution, tAny, tAny], function (self, execution, left, right) {
    if (!self.action) {
      throw new Error('No action registered for operation "' + self.operation + '"');
    }
    return self.action(execution, left, right);
  }]
});

tType.ready(function () {
  tOperation.default = tOperation.new('');
});
