var tEmpty = require('./empty');

var ARG_TYPES = 0;
var FUNCTION  = 1;

var TYPE_TYPE = 0;

var initQueue = [];

var id = 0;
var tType = module.exports = {
  type: TYPE_TYPE,
  id: id,
  name: 'type',
  registry: {'empty': tEmpty},

  is: function (candidate) {
    return candidate.type === TYPE_TYPE
      && candidate.prototype
      && candidate.prototype.type === candidate;
  },

  of: function (candidate) {
    var match = tEmpty;

    Object.keys(tType.registry).forEach(function (type) {
      if (type === 'any') {
        return;
      }

      if (tType.registry[type].is(candidate)) {
        match = type;
      }
    });

    if (match === tEmpty) {
      throw new TypeError('Invalid type found: ' + typeof candidate);
    }

    return match;
  },

  ready: function (fn) {
    if (typeof fn !== 'function') {
      throw new TypeError('argument must be a function');
    }
    initQueue.push(fn);
  },

  init: function () {
    initQueue.forEach(function (fn) {
      fn();
    })
  },

  new: function (name, static, instance) {
    if (typeof name !== 'string') {
      throw new Error('Type name must be a string');
    }

    var type = {
      type: TYPE_TYPE,
      id: ++id,
      name: name
    };

    this.registry[name] = type;

    type.prototype = {
      type: type
    };

    type.multiple = {
      multiple: true,
      type: type
    };

    type.optional = {
      optional: true,
      type: type
    };

    initQueue.push(function () {
      if (typeof static === 'function') {
        static = static();
      }

      Object.keys(static).forEach(function (sName) {
        if (!Array.isArray(static[sName])) {
          throw new Error('Invalid format of ' + name + '.' + sName);
        }

        type[sName] = function () {
          var sourceArgs = Array.prototype.slice.call(arguments);
          var sourceIndex = 0;
          var destArgs = [type.prototype];

          var argTypes = static[sName][ARG_TYPES];
          for (var i = 0; i < argTypes.length; i++) {
            var argType = argTypes[i];

            if (argType.multiple === true) {
              var multiArgs = [];
              destArgs[i + 1] = multiArgs;

              while (sourceIndex < sourceArgs.length && argType.type.is(sourceArgs[sourceIndex])) {
                multiArgs.push(sourceArgs[sourceIndex]);
                sourceIndex++;
              }
              continue;
            }

            if (typeof argType.is !== 'function') {
              throw new Error('Invalid arg type definition provided in '
                + name + '.' + sName);
            }

            if (argType.is(sourceArgs[sourceIndex])) {
              destArgs[i + 1] = sourceArgs[sourceIndex];
              sourceIndex++;
            }

            else {
              throw new Error('Argument ' + i + ' is not of type '
               + argType.name + ' in ' + name + '.' + sName);
            }
          }

          if (sourceIndex < sourceArgs.length) {
            throw new Error('Argument ' + i + ' was not expected '
             + ' in ' + name + '.' + sName);
          }

          if (sourceIndex > sourceArgs.length) {
            throw new Error('Argument ' + i + ' was missing when calling '
             + ' ' + name + '.' + sName);
          }

          return static[sName][FUNCTION].apply(undefined, destArgs);
        };

        type[sName].toString = function () {
          return name + '.' + sName;
        };
      });

      if (typeof instance === 'function') {
        instance = instance();
      }

      if (typeof instance === 'undefined') {
        return;
      }

      Object.keys(instance).forEach(function (pName) {
        if (!Array.isArray(instance[pName])) {
          throw new Error('Invalid format of ' + name + '.' + pName);
        }

        type.prototype[pName] = function () {
          var sourceArgs = Array.prototype.slice.call(arguments);
          var sourceIndex = 0;
          var destArgs = [this];

          var argTypes = instance[pName][ARG_TYPES];
          for (var i = 0; i < argTypes.length; i++) {
            var argType = argTypes[i];

            if (argType.multiple === true) {
              var multiArgs = [];
              destArgs[i + 1] = multiArgs;

              while (sourceIndex < sourceArgs.length && argType.type.is(sourceArgs[sourceIndex])) {
                multiArgs.push(sourceArgs[sourceIndex]);
                sourceIndex++;
              }
              continue;
            }

            if (typeof argType.is !== 'function') {
              throw new Error('Invalid arg type definition provided in <'
                + name + '>.' + pName);
            }

            if (argType.is(sourceArgs[sourceIndex])) {
              destArgs[i + 1] = sourceArgs[sourceIndex];
              sourceIndex++;
            }

            else {
              throw new Error('Argument ' + i + ' is not of type '
               + argType.name + ' in <' + name + '>.' + pName);
            }
          }

          if (sourceIndex < sourceArgs.length) {
            throw new Error('Argument ' + i + ' was not expected '
             + ' in <' + name + '>.' + pName);
          }

          if (sourceIndex > sourceArgs.length) {
            throw new Error('Argument ' + i + ' was missing when calling '
             + ' <' + name + '>.' + pName);
          }

          return instance[pName][FUNCTION].apply(undefined, destArgs);
        };

        type.prototype[pName].toString = function () {
          return '<' + name + '>.' + pName;
        };
      });
    });

    return type;
  }
};
