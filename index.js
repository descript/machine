var tAny        = require('./src/types/any');
var tArray      = require('./src/types/array');
var tBoolean    = require('./src/types/boolean');
var tBreak      = require('./src/types/break');
var tExecution  = require('./src/types/execution');
var tExpression = require('./src/types/expression');
var tFloat      = require('./src/types/float');
var tInteger    = require('./src/types/integer');
var tName       = require('./src/types/name');
var tOperation  = require('./src/types/operation');
var tRoot       = require('./src/types/root');
var tString     = require('./src/types/string');
var tSystem     = require('./src/types/system');
var tType       = require('./src/types/type');

var oCombine    = require('./src/operations/combine');
var oMath       = require('./src/operations/math');

tType.init();

tOperation.register('', oCombine.combine);

tOperation.register('+', oMath.add);
tOperation.register('-', oMath.subtract);
tOperation.register('*', oMath.multiply);
tOperation.register('/', oMath.divide);
tOperation.register('//', oMath.modulus);
tOperation.register('**', oMath.power);
tOperation.register('*/', oMath.root);

var system = tSystem.new();
var mainContext = tRoot.new();
var _break = tBreak.new();

tExpression.new(
  '4 + 5 = ', tExpression.new(
    4, tOperation.new('+'), 5
  ), system.toString, system.lineOut
).execute(mainContext);

tExpression.new(
  tName.new('a'), tOperation.new(':'), 5, _break,
  'Five is: ', tName.new('a'), system.lineOut
).execute(mainContext);

setTimeout(function () {
  console.log('Main context:', mainContext);
}, 1000);

module.exports = mainContext;
